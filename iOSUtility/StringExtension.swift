//
//  LocalizedString.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 10/13/16.
//  Copyright © 2016 D-Link. All rights reserved.
//

import UIKit

extension String {
    public var localized: String {
        return Bundle.main.localizedString(forKey: self, value: self, table: nil)
    }

    public func fontSize(forWidth width: CGFloat) -> CGFloat {
        var size: CGFloat = 0.0
        for i in 1...Int.max {
            if self.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size)]).width > width {
                break
            }
            size = CGFloat(i)
        }
        return size
    }

    public var isEmail: Bool {
        let expression = try? NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: .caseInsensitive)
        return expression?.firstMatch(in: self, options: [], range: NSMakeRange(0, count)) != nil
    }

    public var base64Encoded: String? {
        return Data(self.utf8).base64EncodedString()
    }

    public var base64Decoded: String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }

    public var md5: String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        if let data = data(using: .utf8) as NSData? {
            CC_MD5(data.bytes, CC_LONG(data.count), &digest)
        }
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }

    public subscript(i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    public subscript(bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start..<end]
    }
    public subscript(bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start...end]
    }
    public subscript(bounds: CountablePartialRangeFrom<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(endIndex, offsetBy: -1)
        return self[start...end]
    }
    public subscript(bounds: PartialRangeThrough<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex...end]
    }
    public subscript(bounds: PartialRangeUpTo<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex..<end]
    }
}
