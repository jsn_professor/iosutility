//
//  ViewControllerExtension.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 10/24/16.
//  Copyright © 2016 D-Link. All rights reserved.
//

import HomeKit
import CoreLocation

private var locationManagerDelegate: CLLocationManagerDelegate?
private var locationManager: CLLocationManager?

extension UIViewController {
    private class LocationManagerDelegate: NSObject, CLLocationManagerDelegate {
        private let handler: (CLAuthorizationStatus) -> Void

        init(handler: @escaping (CLAuthorizationStatus) -> Void) {
            self.handler = handler
        }

        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            handler(status)
        }
    }

    public func presentAlert(title: String? = nil,
                             message: String? = nil,
                             buttonText: String? = "alert_button_confirm".localized,
                             tintColor: UIColor = .defaultTint,
                             handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .cancel, handler: handler))
        alert.view.tintColor = tintColor
        present(alert, animated: true)
    }

    public func presentConfirmationAlert(title: String? = nil,
                                         message: String? = nil,
                                         positiveTitle: String? = "alert_button_confirm".localized,
                                         negativeTitle: String? = "alert_button_cancel".localized,
                                         tintColor: UIColor = .defaultTint,
                                         handler: @escaping ((Bool) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: positiveTitle, style: .default) {
            action in
            handler(true)
        })
        alert.addAction(UIAlertAction(title: negativeTitle, style: .cancel) {
            action in
            handler(false)
        })
        alert.view.tintColor = tintColor
        present(alert, animated: true)
    }

    public func presentTextFieldAlert(title: String? = nil,
                                      message: String? = nil,
                                      text: String? = nil,
                                      positiveTitle: String? = "alert_button_confirm".localized,
                                      negativeTitle: String? = "alert_button_cancel".localized,
                                      tintColor: UIColor = .defaultTint,
                                      handler: @escaping ((Bool, String?) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField() {
            textField in
            textField.placeholder = text
            textField.text = text
        }
        alert.addAction(UIAlertAction(title: positiveTitle, style: .default) {
            action in
            handler(true, alert.textFields?.first?.text)
        })
        alert.addAction(UIAlertAction(title: negativeTitle, style: .cancel) {
            action in
            handler(false, alert.textFields?.first?.text)
        })
        alert.view.tintColor = tintColor
        present(alert, animated: true)
    }

    public func presentUnreachableAlert(tintColor: UIColor = .defaultTint) {
        presentConfirmationAlert(title: "alert_network_unreachable_title".localized,
                message: "alert_network_unreachable_content".localized,
                positiveTitle: "alert_button_setting".localized,
                tintColor: tintColor) {
            success in
            if success {
                UIApplication.shared.openSettings()
            }
        }
    }

    public func presentRequestErrorAlert(title: String? = "alert_network_unreachable_title".localized,
                                         message: String? = "alert_network_unreachable_content".localized,
                                         tintColor: UIColor = .defaultTint,
                                         handler: @escaping ((Bool) -> Void)) {
        presentConfirmationAlert(title: title,
                message: message,
                positiveTitle: "alert_button_retry".localized,
                tintColor: tintColor) {
            success in
            handler(success)
        }
    }

//    public func checkPhotosPermission(successHandler: (() -> Void)? = nil, failHandler: (() -> Void)? = nil) {
//        if PHPhotoLibrary.authorizationStatus() == .authorized {
//            successHandler?()
//        } else {
//            let showAlert = PHPhotoLibrary.authorizationStatus() != .notDetermined
//            if #available(iOS 14, *) {
//                PHPhotoLibrary.requestAuthorization(for: .readWrite) {
//                    status in
//                    DispatchQueue.main.async {
//                        if status == .authorized {
//                            successHandler?()
//                        } else {
//                            if showAlert {
//                                self.presentConfirmationAlert(title: "alert_permission_required_title".localized,
//                                        message: (Bundle.main.infoDictionary?["NSPhotoLibraryAddUsageDescription"] ?? Bundle.main.infoDictionary?["NSPhotoLibraryUsageDescription"]) as? String) {
//                                    success in
//                                    if success {
//                                        UIApplication.shared.openAppSettings()
//                                    }
//                                    failHandler?()
//                                }
//                            } else {
//                                failHandler?()
//                            }
//                        }
//                    }
//                }
//            } else {
//                PHPhotoLibrary.requestAuthorization() {
//                    status in
//                    DispatchQueue.main.async {
//                        if status == .authorized {
//                            successHandler?()
//                        } else {
//                            if showAlert {
//                                self.presentConfirmationAlert(title: "alert_permission_required_title".localized, message: Bundle.main.infoDictionary?["NSPhotoLibraryUsageDescription"] as? String) {
//                                    success in
//                                    if success {
//                                        UIApplication.shared.openAppSettings()
//                                    }
//                                    failHandler?()
//                                }
//                            } else {
//                                failHandler?()
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

//    public func checkCameraPermission(successHandler: (() -> Void)? = nil, failHandler: (() -> Void)? = nil) {
//        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .authorized {
//            successHandler?()
//        } else {
//            let showAlert = AVCaptureDevice.authorizationStatus(for: AVMediaType.video) != .notDetermined
//            AVCaptureDevice.requestAccess(for: AVMediaType.video) {
//                status in
//                DispatchQueue.main.async {
//                    if status {
//                        successHandler?()
//                    } else {
//                        if showAlert {
//                            self.presentConfirmationAlert(title: "alert_permission_required_title".localized, message: Bundle.main.infoDictionary?["NSCameraUsageDescription"] as? String) {
//                                success in
//                                if success {
//                                    UIApplication.shared.openAppSettings()
//                                }
//                                failHandler?()
//                            }
//                        } else {
//                            failHandler?()
//                        }
//                    }
//                }
//            }
//        }
//    }

//    public func checkMicrophonePermission(successHandler: (() -> Void)? = nil, failHandler: (() -> Void)? = nil) {
//        if AVAudioSession.sharedInstance().recordPermission == .granted {
//            successHandler?()
//        } else {
//            let showAlert = AVAudioSession.sharedInstance().recordPermission != .undetermined
//            AVAudioSession.sharedInstance().requestRecordPermission() {
//                status in
//                DispatchQueue.main.async {
//                    if status {
//                        successHandler?()
//                    } else {
//                        if showAlert {
//                            self.presentConfirmationAlert(title: "alert_permission_required_title".localized, message: Bundle.main.infoDictionary?["NSMicrophoneUsageDescription"] as? String) {
//                                success in
//                                if success {
//                                    UIApplication.shared.openAppSettings()
//                                }
//                                failHandler?()
//                            }
//                        } else {
//                            failHandler?()
//                        }
//                    }
//                }
//            }
//        }
//    }

//    public func checkLocationPermission(successHandler: (() -> Void)? = nil, failHandler: (() -> Void)? = nil) {
//        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
//                   CLLocationManager.authorizationStatus() == .authorizedAlways {
//            successHandler?()
//        } else {
//            let showAlert = CLLocationManager.authorizationStatus() != .notDetermined
//            let lastStatus = CLLocationManager.authorizationStatus()
//            locationManagerDelegate = LocationManagerDelegate() {
//                status in
//                if status == .authorizedWhenInUse ||
//                           status == .authorizedAlways {
//                    successHandler?()
//                    locationManagerDelegate = nil
//                    locationManager = nil
//                } else {
//                    if showAlert {
//                        self.presentConfirmationAlert(title: "alert_permission_required_title".localized, message: Bundle.main.infoDictionary?["NSLocationAlwaysAndWhenInUseUsageDescription"] as? String) {
//                            success in
//                            if success {
//                                UIApplication.shared.openAppSettings()
//                            }
//                            failHandler?()
//                        }
//                        locationManagerDelegate = nil
//                        locationManager = nil
//                    } else if status != lastStatus {
//                        failHandler?()
//                        locationManagerDelegate = nil
//                        locationManager = nil
//                    }
//                }
//            }
//            locationManager = CLLocationManager()
//            locationManager?.delegate = locationManagerDelegate
//            locationManager?.requestAlwaysAuthorization()
//        }
//    }

//    public func checkNotificationPermission(options: UNAuthorizationOptions = [.sound, .alert], successHandler: (() -> Void)? = nil, failHandler: (() -> Void)? = nil) {
//        UNUserNotificationCenter.current().getNotificationSettings() {
//            settings in
//            if settings.authorizationStatus == .authorized {
//                DispatchQueue.main.async {
//                    successHandler?()
//                }
//            } else {
//                let showAlert = settings.authorizationStatus != .notDetermined
//                UNUserNotificationCenter.current().requestAuthorization(options: options) {
//                    success, error in
//                    DispatchQueue.main.async {
//                        if success {
//                            successHandler?()
//                        } else {
//                            if showAlert {
//                                self.presentConfirmationAlert(title: "alert_permission_required_title".localized, message: "alert_permission_notification".localized) {
//                                    success in
//                                    if success {
//                                        UIApplication.shared.openAppSettings()
//                                    }
//                                    failHandler?()
//                                }
//                            } else {
//                                failHandler?()
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    public func getDirection(with location: (latitude: String, longitude: String), mode: String) {
        if let url = URL(string: "comgooglemaps://?daddr=\(location.latitude),\(location.longitude)&directionsmode=\(mode)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        } else if let url = URL(string: "https://www.google.com/maps/dir/?api=1&destination=\(location.latitude),\(location.longitude)&travelmode=\(mode)") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    public func getDirection(with query: String) {
        let query = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL(string: "comgooglemaps://?q=\(query)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        } else if let url = URL(string: "https://www.google.com/maps/search/?api=1&query=\(query)") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
