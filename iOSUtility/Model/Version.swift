//
//  Version.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 3/31/17.
//  Copyright © 2017 D-Link. All rights reserved.
//

import UIKit

public struct Version: Comparable {
    public static var current: Version {
        return Version(version: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)
    }
    public var major: Int?
    public var minor: Int?
    public var build: Int?

    public init(version: String?) {
        guard let version = version else {
            return
        }
        let split = version.trimmingCharacters(in: .whitespaces).components(separatedBy: ".")
        for value in split {
            if let version = Int(value) {
                if major == nil {
                    major = version
                } else if minor == nil {
                    minor = version
                } else if build == nil {
                    build = version
                }
            }
        }
    }

    public func toString() -> String {
        var versionString = ""
        if let major = major {
            versionString.append("\(major)")
        }
        if let minor = minor {
            versionString.append(".\(minor)")
        }
        if let build = build {
            versionString.append(".\(build)")
        }
        return versionString
    }

    public static func ==(lhs: Version, rhs: Version) -> Bool {
        return lhs.major == rhs.major && lhs.minor == rhs.minor && lhs.build == rhs.build
    }

    public static func >(lhs: Version, rhs: Version) -> Bool {
        if let leftMajor = lhs.major, let rightMajor = rhs.major {
            if leftMajor > rightMajor {
                return true
            } else if leftMajor == rightMajor, let leftMinor = lhs.minor, let rightMinor = rhs.minor {
                if leftMinor > rightMinor {
                    return true
                } else if leftMinor == rightMinor, let leftBuild = lhs.build, let rightBuild = rhs.build {
                    if leftBuild > rightBuild {
                        return true
                    }
                }
            }
        }
        return false
    }

    public static func <(lhs: Version, rhs: Version) -> Bool {
        if let leftMajor = lhs.major, let rightMajor = rhs.major {
            if leftMajor < rightMajor {
                return true
            } else if leftMajor == rightMajor, let leftMinor = lhs.minor, let rightMinor = rhs.minor {
                if leftMinor < rightMinor {
                    return true
                } else if leftMinor == rightMinor, let leftBuild = lhs.build, let rightBuild = rhs.build {
                    if leftBuild < rightBuild {
                        return true
                    }
                }
            }
        }
        return false
    }
}
