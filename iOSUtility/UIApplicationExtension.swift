//
//  ApplicationExtension.swift
//  iOSTemplate
//
//  Created by Jason Hsu 08329 on 23/04/2017.
//  Copyright © 2017 Agrowood. All rights reserved.
//

import UIKit

extension UIApplication {
    public func replace(rootViewControllerWith viewController: UIViewController) {
        if let window = keyWindow {
            let oldRootViewController = window.rootViewController
            UIView.transition(
                    with: window,
                    duration: 0.3,
                    options: .transitionCrossDissolve,
                    animations: {
                        let animationsEnabled = UIView.areAnimationsEnabled
                        UIView.setAnimationsEnabled(false)
                        window.rootViewController = viewController
                        UIView.setAnimationsEnabled(animationsEnabled)
                    }) {
                finished in
                oldRootViewController?.dismiss(animated: false)
            }
        }
    }

    public func handleUrl(url: URL, applicationId: String? = nil) {
        if canOpenURL(url) {
            open(url)
        } else if let applicationId = applicationId,
                  let url = URL(string: "https://apps.apple.com/app/\(applicationId)") {
            open(url)
        }
    }

    public func openAppSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString), canOpenURL(url) {
            open(url)
        }
    }

    public func openSettings() {
        if let url = URL(string: "App-prefs:"), canOpenURL(url) {
            open(url)
        }
    }
}
