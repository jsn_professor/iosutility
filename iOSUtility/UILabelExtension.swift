//
//  LabelExtension.swift
//  Omna
//
//  Created by Professor on 21/07/2017.
//  Copyright © 2017 D-Link. All rights reserved.
//

import UIKit

extension UILabel {
    public var isTruncated: Bool {
        guard let labelText = text else {
            return false
        }
        let attributes: [NSAttributedString.Key: Any]?
        if let font = font {
            attributes = [.font: font]
        } else {
            attributes = nil
        }
        let labelTextSize = (labelText as NSString).boundingRect(
                with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
                options: .usesLineFragmentOrigin,
                attributes: attributes,
                context: nil).size
        return labelTextSize.height > bounds.size.height
    }

    public func setText(text: String?) {
        if text != nil {
            alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
        self.text = text
    }
}
